# Focus on the Family Connect Four Interview Exercise #
## By Howard Owsley ##

## Design Decisions ##

Given the short amount of time to complete the exercise, a full study of available approaches was not done. I used what I knew and prioritized what I did not. Over the past year I have been working in Spring Boot at my paid job and React JS with node express for a project on my off time. Thus it was a logical choice to use Spring Boot Java for the back end and React for the front end. Although my experience in web UI development is far less then my server side experience, React does a pretty good job of making it easy. I have some experience in REST and SOAP APIs that only send data to the web UI in response to a request. However, I had never worked with a server side app that contacts the web UI at will. Thus, with short research, I chose to use STOMP over web sockets which is supported well in both React and Spring Boot. It is also supposed to be compatible with all modern browsers; emphasis on the ‘supposed to’. 

## Items of note ##

I don’t want to assume knowledge, or lack of knowledge, in any technology used but I would like to bring up some things that may not be known to those that do not use React or Spring Boot

### React ###

This is my second react app so I am still learning. 
- I created the app using the ‘create-react-app’ command that puts a lot of boilerplate code into your project. 
  - All the code I wrote is under the ‘src’ folder
  - I tried to clean up some of the boilerplate code in the root directory but I left the ‘public’ folder as is.
- I chose to use TypeScript over normal JavaScript; it tends to be more verbose but, in my opinion, more clear and maintainable.
- I believe the code is pretty standard for React

### Spring Boot ###

Spring Boot has a lot of ‘magic happens here’ things going so it is much less code then normal Java apps. It has quite a few of tool built in that are commonly seen in Spring Boot apps
- Lombok – Handles all the boilerplate code for POJO getters, setters, and constructors as well a builder construct.
- Spring JPA – Persistence tools that allow you to define database tables in the ‘Entity’ classes and Repositorys that have numerous per-existing query functions as well as a sort of auto naming for query functions based on the entity class.
- The dependency injection concepts of Spring allows you to only write the code pieces needed as can be seen with the STOMP config and disconnect event handling.
- Configuration for Spring Boot is in the src/main/resources/application.yml file

Last weekend, after our interview and before this assignment, I did play a little bit with C# and it has a lot of similarities to Spring Boot. In fact I even saw a comment on the internet saying the same.- I created the app using the ‘create-react-app’ command that puts a lot of boilerplate code into your project. 

### Heroku ###

It was my plan from the beginning to host my game on Heroku. That influenced my decision to use PostgreSQL since it is installed ‘out of the box’ on Heroku. There were some code impacts driven by Heroku.
- There are two database configuration for Spring Boot that need to be switched depending on where the code is running
- There is a configuration on UI side at the top of the GamePage.tsx file. The BROKER_URL needs to be changed to point to the appropriate server. 
  - The UI can also be ran locally and hit the server on Heroku for the API
- The keep alive code was added, as recommended by Heroku, to overcome their auto disconect logic that also effects web sockets- I created the app using the ‘create-react-app’ command that puts a lot of boilerplate code into your project. 
- Heroku shuts down the 'dyno', or sever, when not being used so there is a bit of delay the fist time using it after it has sat dormant.

### Browser Compatibility ###

Although web sockets are supposed to available in all modern browsers I could not get to connect on Microsoft edge or Internet Explorer. I did all my initial testing in Chrome so I did not discover this issue until the very end.

The app was tested on Chrome, Firefox, Safari, and even Chrome on Android 
