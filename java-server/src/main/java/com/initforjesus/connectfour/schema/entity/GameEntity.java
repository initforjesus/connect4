package com.initforjesus.connectfour.schema.entity;

import com.initforjesus.connectfour.schema.dto.PlayerColor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Java Persistence Entity defining one row in the games table
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "games")
public class GameEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // V4 UUIDs are 36 characters long
    @Column(name = "red_user_id", length = 36)
    private String redUserId;
    @Column(name = "yellow_user_id", length = 36)
    private String yellowUserId;
    @Column(name = "red_session_id", length = 36)
    private String redSessionId;
    @Column(name = "yellow_session_id", length = 36)
    private String yellowSessionId;

    @Column(name = "next_move")
    private PlayerColor nextMove;

    @Column(name = "is_waiting")
    private Boolean isWaiting;

    @Column(name = "last_action_ts")
    private Instant lastActionTs;

    @ToString.Exclude
    @OneToMany(mappedBy="gameEntity", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<LocationEntity> LocationEntities = new ArrayList<>();
}
