package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The chip location on a grid 6 columns with 7 rows high. NOTE: this is pivoted from the original connect four game is.
 * Columns are A-F (lt to rt)and rows are 1-7 (bot to top) to make the enum value (i.e. row 1 of the 2nd column is B1).
 * Each location also identifies what win lines that the location resides in with null for line less then 4 in length.
 */
@AllArgsConstructor
@Getter
public enum ChipLocation {
    // 6 columns with 7 rows high - pivot of original game
    // Columns are A-F (lt to rt)and rows are 1-7 (bot to top) to make the enum value i.e. row 1 of 2nd column is B1
    A1(1, 1, 4, null),
    A2(2, 1, 3, null),
    A3(3, 1, 2, null),
    A4(4, 1, 1, 1),
    A5(5, 1, null, 2),
    A6(6, 1, null, 3),
    A7(7, 1, null, 4),
    B1(1, 2, 5, null),
    B2(2, 2, 4, null),
    B3(3, 2, 3, 1),
    B4(4, 2, 2, 2),
    B5(5, 2, 1, 3),
    B6(6, 2, null, 4),
    B7(7, 2, null, 5),
    C1(1, 3, 6, null),
    C2(2, 3, 5, 1),
    C3(3, 3, 4, 2),
    C4(4, 3, 3, 3),
    C5(5, 3, 2, 4),
    C6(6, 3, 1, 5),
    C7(7, 3, null, 6),
    D1(1, 4, null, 1),
    D2(2, 4, 6, 2),
    D3(3, 4, 5, 3),
    D4(4, 4, 4, 4),
    D5(5, 4, 3, 5),
    D6(6, 4, 2, 6),
    D7(7, 4, 1, null),
    E1(1, 5, null, 2),
    E2(2, 5, null, 3),
    E3(3, 5, 6, 4),
    E4(4, 5, 5, 5),
    E5(5, 5, 4, 6),
    E6(6, 5, 3, null),
    E7(7, 5, 2, null),
    F1(1, 6, null, 3),
    F2(2, 6, null, 4),
    F3(3, 6, null, 5),
    F4(4, 6, 6, 6),
    F5(5, 6, 5, null),
    F6(6, 6, 4, null),
    F7(7, 6, 3, null);

    private final Integer row;
    private final Integer column;
    private final Integer rightDiagonal;
    private final Integer leftDiagonal;
}
