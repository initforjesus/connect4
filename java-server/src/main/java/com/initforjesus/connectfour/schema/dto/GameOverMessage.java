package com.initforjesus.connectfour.schema.dto;

/**
 * Specific, or constant, game over messages sent to user via the User Notification DTO
 */
public interface GameOverMessage {
    String YOU_WON = "YOU_WON";
    String YOU_LOST = "YOU_LOST";
    String DRAW = "DRAW";
    String OPPONENT_QUIT = "OPPONENT_QUIT";
}
