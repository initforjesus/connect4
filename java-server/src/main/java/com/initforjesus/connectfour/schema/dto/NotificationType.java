package com.initforjesus.connectfour.schema.dto;

/**
 * Notification types sent to user via the User Notification DTO
 */
public enum NotificationType {
    OPPONENT_JOINED, // If waiting and other player goes first
    YOUR_MOVE, // To start game with first move
    GAME_OVER, //Won, Loss, Player Quit - in message
    ERROR
}
