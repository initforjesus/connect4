package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StartGameResponseDto {
    private String userId;
    private Long gameId;
    private PlayerColor playerColor;
    private PlayerColor goesFirst;
    private boolean waiting;

}
