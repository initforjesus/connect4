package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User Notification DTO
 * Used to send various messages to the user based on the Notification Type. Messages can be free form or constant
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserNotificationDto {
    private String userId;
    private Long gameId;
    private NotificationType type;
    private String message;
}
