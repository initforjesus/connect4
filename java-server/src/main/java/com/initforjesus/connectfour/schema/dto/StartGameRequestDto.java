package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request sent by the user to the server to request starting, or joining, a gamr
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StartGameRequestDto {
    private String userId;
}
