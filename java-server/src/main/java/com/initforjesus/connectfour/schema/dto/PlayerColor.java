package com.initforjesus.connectfour.schema.dto;

public enum PlayerColor {
    RED,
    YELLOW
}
