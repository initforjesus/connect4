package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * response sent to the other player after a move has been recorded and processed
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MoveResponseDto {
    private String userId;
    private Long gameId;
    private ChipLocation chipLocation;
}
