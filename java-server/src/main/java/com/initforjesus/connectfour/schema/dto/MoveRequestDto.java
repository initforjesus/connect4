package com.initforjesus.connectfour.schema.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request sent from user to the server whenever a move is made
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MoveRequestDto {
    private String userId;
    private Long gameId;
    private PlayerColor playerColor;
    private ChipLocation chipLocation;
}
