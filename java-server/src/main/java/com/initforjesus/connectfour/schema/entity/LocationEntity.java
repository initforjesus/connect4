package com.initforjesus.connectfour.schema.entity;

import com.initforjesus.connectfour.schema.dto.ChipLocation;
import com.initforjesus.connectfour.schema.dto.PlayerColor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Java Persistence Entity defining one row in the locations table
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "locations")
public class LocationEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    private GameEntity gameEntity;

    @Column(name = "player_color")
    private PlayerColor playerColor;

    @Column(name = "chip_location")
    private ChipLocation chipLocation;

    // win lines
    @Column(name = "row_line")
    private Integer rowLine;
    @Column(name = "column_line")
    private Integer columnLine;
    @Column(name = "rightDiagonal")
    private Integer rightDiagonal;
    @Column(name = "leftDiagonal")
    private Integer leftDiagonal;
}
