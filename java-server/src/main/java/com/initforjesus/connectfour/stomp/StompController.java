package com.initforjesus.connectfour.stomp;

import com.initforjesus.connectfour.schema.dto.ChipLocation;
import com.initforjesus.connectfour.schema.dto.GameOverMessage;
import com.initforjesus.connectfour.schema.dto.MoveRequestDto;
import com.initforjesus.connectfour.schema.dto.MoveResponseDto;
import com.initforjesus.connectfour.schema.dto.NotificationType;
import com.initforjesus.connectfour.schema.dto.PlayerColor;
import com.initforjesus.connectfour.schema.dto.StartGameRequestDto;
import com.initforjesus.connectfour.schema.dto.StartGameResponseDto;
import com.initforjesus.connectfour.schema.dto.UserNotificationDto;
import com.initforjesus.connectfour.schema.entity.GameEntity;
import com.initforjesus.connectfour.service.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.transaction.Transactional;

import static com.initforjesus.connectfour.schema.dto.GameOverMessage.DRAW;
import static com.initforjesus.connectfour.schema.dto.GameOverMessage.YOU_LOST;
import static com.initforjesus.connectfour.schema.dto.GameOverMessage.YOU_WON;

/**
 * Stomp/Websocket controller class
 * This will handle all interactions with stomp/websocket including endpoints and event listeners
 */
@Controller
@Slf4j
public class StompController implements ApplicationListener<SessionDisconnectEvent> {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final GameService gameService;

    public StompController(SimpMessagingTemplate simpMessagingTemplate, GameService gameService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.gameService = gameService;
    }

    /**
     * Handle the disconnect event by using the session id to find and end the game. Then try to notify appropriate
     * game participants
     *
     * @param event SessionDisconnectEvent caught by event listener
     */
    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {

        String sessionId = event.getSessionId();
        GameEntity gameEntity = gameService.dropDisconnectedGame(sessionId);

        if (gameEntity != null) {
            String droppedUser = sessionId.equals(gameEntity.getRedSessionId()) ? gameEntity.getRedUserId() : gameEntity.getYellowUserId();

            if (!gameEntity.getIsWaiting()) {
                String otherUser = sessionId.equals(gameEntity.getRedSessionId()) ? gameEntity.getYellowUserId() : gameEntity.getRedUserId();

                if (otherUser != null) {
                    log.debug("GameId {} lost player {}, notifying remaining user {}",
                            gameEntity.getId(), droppedUser, otherUser);
                    sendUserNotification(otherUser, gameEntity.getId(), NotificationType.GAME_OVER, GameOverMessage.OPPONENT_QUIT);
                } else {
                    log.error("GameId {} dropped a player but could not find other user", gameEntity.getId());
                    // try to send errors
                    if (gameEntity.getRedUserId() != null) {
                        sendUserNotification(gameEntity.getRedUserId(), gameEntity.getId(), NotificationType.ERROR);
                    }
                    if (gameEntity.getYellowUserId() != null) {
                        sendUserNotification(gameEntity.getYellowUserId(), gameEntity.getId(), NotificationType.ERROR);
                    }
                }
            } else {
                log.debug("GameId {} player {} disconnected from a game that had not started yet",
                        gameEntity.getId(), droppedUser);
            }
        }
    }

    /**
     * Handle game start request
     * Assign the player to a new game or an existing game and send them the game info. If it is an existing game,
     * notify the other player with the appropriate response.
     *
     * @param startGameRequestDto game start request data
     * @param headerAccessor header containing session Id
     */
    @MessageMapping("/start")
    public void startGame(@Payload StartGameRequestDto startGameRequestDto, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor != null ? headerAccessor.getSessionId() : null;
        String userId = startGameRequestDto!= null ? startGameRequestDto.getUserId() : null;
        log.debug("StartGame request for userId: {} and sessionId: {}", userId, sessionId);

        GameEntity gameEntity = gameService.processStartRequest(startGameRequestDto, sessionId);
        if (gameEntity != null ) {
            PlayerColor color = userId.equals(gameEntity.getRedUserId()) ? PlayerColor.RED : PlayerColor.YELLOW;
            StartGameResponseDto startGameResponseDto = StartGameResponseDto.builder()
                    .gameId(gameEntity.getId())
                    .userId(userId)
                    .playerColor(color)
                    .goesFirst(gameEntity.getNextMove())
                    .waiting(gameEntity.getIsWaiting())
                    .build();
            simpMessagingTemplate.convertAndSendToUser(userId, "/queue/start", startGameResponseDto);
            log.debug("sent start dto to userId: {} with responseDto: {}", userId, startGameResponseDto.toString());

            if (!gameEntity.getIsWaiting()) {
                log.debug("Added user with Id: {} to gameId: {} as {}", userId, gameEntity.getId(), color.name());

                String otherPlayer = color.equals(PlayerColor.RED) ? gameEntity.getYellowUserId() : gameEntity.getRedUserId();
                if (gameEntity.getNextMove().equals(color)) {
                    sendUserNotification(otherPlayer, gameEntity.getId(), NotificationType.OPPONENT_JOINED);
                } else {
                    sendUserNotification(otherPlayer, gameEntity.getId(), NotificationType.YOUR_MOVE);
                }
            } else {
                log.debug("created new game with Id: {} and userId: {} as {}", gameEntity.getId(), userId, color.name());
            }
        } else {
            sendUserNotification(userId, 0L, NotificationType.ERROR);
        }
    }

    /**
     * Handle move request
     * Validate and store move request and determine if there is win/game over condition. Send a move response to the
     * other player indicating the move and if there is a WIN or DRAW, notify game participants as appropriate
     *
     * @param moveRequestDto move request data
     * @param headerAccessor header containing session Id
     */
    @MessageMapping("/move")
    @Transactional
    public void playerMove(@Payload MoveRequestDto moveRequestDto, SimpMessageHeaderAccessor headerAccessor) {
        log.debug("MoveRequest: {}", moveRequestDto.toString());
        String sessionId = headerAccessor.getSessionId();

        GameEntity gameEntity = gameService.processMoveRequest(moveRequestDto, sessionId);
        if (gameEntity != null) {
            ChipLocation chipLocation = moveRequestDto.getChipLocation();
            String opponentUserId = moveRequestDto.getPlayerColor().equals(PlayerColor.RED) ? gameEntity.getYellowUserId() : gameEntity.getRedUserId();
            log.debug("sending move response to userId {} with chipLocation {}", opponentUserId, chipLocation);
            simpMessagingTemplate.convertAndSendToUser(opponentUserId, "/queue/move", MoveResponseDto.builder()
                    .userId(opponentUserId)
                    .gameId(gameEntity.getId())
                    .chipLocation(chipLocation)
                    .build());

            String winCondition = gameService.checkForWinCondition(gameEntity, chipLocation, moveRequestDto.getPlayerColor());
            if (YOU_WON.equals(winCondition)) {
                sendUserNotification(moveRequestDto.getUserId(), gameEntity.getId(), NotificationType.GAME_OVER, YOU_WON);
                sendUserNotification(opponentUserId, gameEntity.getId(), NotificationType.GAME_OVER, YOU_LOST);
            } else if (DRAW.equals(winCondition)) {
                sendUserNotification(gameEntity.getRedUserId(), gameEntity.getId(), NotificationType.GAME_OVER, DRAW);
                sendUserNotification(gameEntity.getYellowUserId(), gameEntity.getId(), NotificationType.GAME_OVER, DRAW);
            }
        } else {
            log.debug("Error processing MoveRequest {}", moveRequestDto.toString());
            if (moveRequestDto.getUserId() != null) {
                sendUserNotification(moveRequestDto.getUserId(), 0L, NotificationType.ERROR);
            }
        }
    }

    /**
     * Keep alive function needed for server like Heroku that tries to disconnect on a timed basis
     * @param message irrelevant message
     */
    @MessageMapping("/keepAlive")
    public void keepAlive(@Payload String message) {
        log.debug(message);
    }


    private void sendUserNotification(String userId, Long gameId, NotificationType type) {
        sendUserNotification(userId, gameId, type, null);
    }
    private void sendUserNotification(String userId, Long gameId, NotificationType type, String message) {
        UserNotificationDto userNotificationDto = UserNotificationDto.builder()
                .userId(userId)
                .gameId(gameId)
                .type(type)
                .message(message)
                .build();
        log.debug("Sending notification message to userId {} payload: {}", userId, userNotificationDto.toString());
        simpMessagingTemplate.convertAndSendToUser(userId, "/queue/alert", userNotificationDto);
    }
}
