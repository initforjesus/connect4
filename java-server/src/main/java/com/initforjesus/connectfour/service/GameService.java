package com.initforjesus.connectfour.service;

import com.initforjesus.connectfour.repository.GameRepository;
import com.initforjesus.connectfour.repository.LocationRepository;
import com.initforjesus.connectfour.schema.dto.ChipLocation;
import com.initforjesus.connectfour.schema.dto.MoveRequestDto;
import com.initforjesus.connectfour.schema.dto.PlayerColor;
import com.initforjesus.connectfour.schema.dto.StartGameRequestDto;
import com.initforjesus.connectfour.schema.entity.GameEntity;
import com.initforjesus.connectfour.schema.entity.LocationEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.initforjesus.connectfour.schema.dto.GameOverMessage.DRAW;
import static com.initforjesus.connectfour.schema.dto.GameOverMessage.YOU_WON;

/**
 * Handle all the game processes separate from the transport mechanism
 */
@Service
@Slf4j
public class GameService {
    private static final int ROW_COUNT = 7;
    private static final int COLUMN_COUNT = 6;

    private Random random;

    private final GameRepository gameRepository;
    private final LocationRepository locationRepository;

    public GameService(GameRepository gameRepository, LocationRepository locationRepository) {
        this.gameRepository = gameRepository;
        this.locationRepository = locationRepository;
        this.random = new Random();
    }

    /**
     * Get and delete the game record based on the session ID
     *
     * @param sessionId sessionId that disconnected
     * @return game entity that was deleted or NULL if there was n error
     */
    public GameEntity dropDisconnectedGame(String sessionId) {
        GameEntity gameEntity = gameRepository.findBySessionId(sessionId).orElse(null);
        if (gameEntity != null) {
            gameRepository.delete(gameEntity);
            log.debug("deleted game {}", gameEntity.toString());
        }
        return gameEntity;
    }

    /**
     * Create a new game or assign the player to an existing game. For a new game, the player color and who goes first
     * are determined randomly. The session Id is also stored with the userId for later validation and disconnects
     *
     * @param startGameRequestDto Start Request
     * @param sessionId Session Id of the start reque
     * @return game entity for the new game or NULL is there is an error
     */
    public GameEntity processStartRequest(StartGameRequestDto startGameRequestDto, String sessionId) {
        if (startGameRequestDto == null) {
            log.error("startGameRequestDto is null");
            return null;
        }
        String userId = startGameRequestDto.getUserId();

        if (userId == null) {
            log.error("UserId is null in StartGameRequestDto: {}", startGameRequestDto);
            return null;
        }
        if (sessionId == null) {
            log.error("SessionId is null for StartGameRequestDto: {}", startGameRequestDto);
            return null;
        }

        GameEntity gameEntity = gameRepository.findFirstByIsWaitingTrue().orElse(GameEntity.builder().build());

        if (userId.equals(gameEntity.getYellowUserId()) || userId.equals(gameEntity.getRedUserId())) {
            log.debug("User Id {} is already in game {}", userId, gameEntity.getId());
            return gameEntity;
        }

        if (gameEntity.getNextMove() == null) { // new game
            gameEntity.setIsWaiting(true);
            gameEntity.setNextMove(random.nextBoolean() ? PlayerColor.RED : PlayerColor.YELLOW);
            PlayerColor color = random.nextBoolean() ? PlayerColor.RED : PlayerColor.YELLOW;
            if (PlayerColor.RED.equals(color)) {
                gameEntity.setRedUserId(userId);
                gameEntity.setRedSessionId(sessionId);
            } else {
                gameEntity.setYellowUserId(userId);
                gameEntity.setYellowSessionId(sessionId);
            }
        } else { // Add player to existing game
            gameEntity.setIsWaiting(false);
            if (gameEntity.getRedUserId() == null) {
                gameEntity.setRedUserId(userId);
                gameEntity.setRedSessionId(sessionId);
            } else {
                gameEntity.setYellowUserId(userId);
                gameEntity.setYellowSessionId(sessionId);
            }
        }
        gameEntity.setLastActionTs(Instant.now());
        gameRepository.save(gameEntity);

        return gameEntity;
    }

    /**
     * Validate the request and store the move
     *
     * @param moveRequestDto The requested move data
     * @param sessionId Session Id of the move request
     * @return game entity with the move added or NULL is there is an error
     */
    public GameEntity processMoveRequest(MoveRequestDto moveRequestDto, String sessionId) {
        GameEntity gameEntity = gameRepository.findById(moveRequestDto.getGameId()).orElse(null);
        if (gameEntity == null) {
            log.debug("Game entity not found for gameId {}", moveRequestDto.getGameId());
            return null;
        }
        if (isValidMessage(moveRequestDto, gameEntity, sessionId)) {
            ChipLocation chipLocation = moveRequestDto.getChipLocation();
            gameEntity.setLastActionTs(Instant.now());
            gameEntity.setNextMove(gameEntity.getNextMove().equals(PlayerColor.RED) ? PlayerColor.YELLOW : PlayerColor.RED);
            gameEntity.getLocationEntities().add(LocationEntity.builder()
                    .gameEntity(gameEntity)
                    .playerColor(moveRequestDto.getPlayerColor())
                    .chipLocation(chipLocation)
                    .columnLine(chipLocation.getColumn())
                    .rowLine(chipLocation.getRow())
                    .rightDiagonal(chipLocation.getRightDiagonal())
                    .leftDiagonal(chipLocation.getLeftDiagonal())
                    .build());
            gameRepository.save(gameEntity);

            return gameEntity;
        } else {
            return null;
        }
    }

    /**
     *
     * @param gameEntity game entity with all current moves in it
     * @param chipLocation the chip location of the current move
     * @param playerColor the player color to check for win condition
     * @return string represent the win condition as NONE, YOU_WON, or DRAW
     */
    public String checkForWinCondition(GameEntity gameEntity, ChipLocation chipLocation, PlayerColor playerColor) {
        String winCondition = "NONE";

        // make a list of all the row ordered win lines
        List<List<LocationEntity>> winLines = new ArrayList<>();
        winLines.add(
                locationRepository.findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, chipLocation.getColumn()));
        if (chipLocation.getLeftDiagonal() != null) {
            winLines.add(
                    locationRepository.findAllByGameEntityAndLeftDiagonalOrderByRowLine(gameEntity, chipLocation.getLeftDiagonal()));
        }
        if (chipLocation.getRightDiagonal() != null) {
            winLines.add(
                    locationRepository.findAllByGameEntityAndRightDiagonalOrderByRowLine(gameEntity, chipLocation.getRightDiagonal()));
        }

        List<LocationEntity> rowLine =
                locationRepository.findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity, chipLocation.getRow());

        if (isWinInList(winLines, playerColor, true) || isWin(rowLine, playerColor, false)) {
            log.debug("{} is the winner of game {}", playerColor, gameEntity.getId());
            winCondition = YOU_WON;
            gameRepository.delete(gameEntity);
        } else if ((chipLocation.getRow() == ROW_COUNT && rowLine.size() == COLUMN_COUNT)) {
            log.debug("Game {} is a draw", gameEntity.getId());
            winCondition = DRAW;
            gameRepository.delete(gameEntity);
        }
        return winCondition;
    }

    private boolean isValidMessage(MoveRequestDto moveRequestDto, GameEntity gameEntity, String sessionId) {
        boolean isValid = true;

        if (sessionId == null) {
            isValid = false;
            log.debug("Session Id is null");
        }
        if (isValid && moveRequestDto.getUserId() == null) {
            isValid = false;
            log.debug("UserId is null");
        }
        if (isValid && moveRequestDto.getPlayerColor() == null) {
            isValid = false;
            log.debug("Player Color is null");
        }
        if (isValid && moveRequestDto.getChipLocation() == null) {
            isValid = false;
            log.debug("Chip location is null");
        }
        if (isValid) {
            String storedSessionId = PlayerColor.RED.equals(moveRequestDto.getPlayerColor()) ?
                    gameEntity.getRedSessionId() : gameEntity.getYellowSessionId();
            if (!sessionId.equals(storedSessionId)){
                isValid = false;
                log.debug("Provided sessionId {} does not equal stored sessionId {}", sessionId, storedSessionId);
            }
        }
        if(isValid) {
            String storedUserId = PlayerColor.RED.equals(moveRequestDto.getPlayerColor()) ?
                    gameEntity.getRedUserId() : gameEntity.getYellowUserId();
            if (!moveRequestDto.getUserId().equals(storedUserId)){
                isValid = false;
                log.debug("Provided userId {} does not equal stored userId {}", moveRequestDto.getUserId(), storedUserId);
            }
        }
        if (isValid && !moveRequestDto.getPlayerColor().equals(gameEntity.getNextMove())) {
            isValid = false;
            log.debug("Player color {} is not equal to next move color {}",
                    moveRequestDto.getPlayerColor(), gameEntity.getNextMove());
        }
        if (isValid){
            ChipLocation location = moveRequestDto.getChipLocation();
            int nextAvailableColumnSlot = locationRepository.findAllByGameEntityAndColumnLineOrderByRowLine(
                    gameEntity, location.getColumn()).size() + 1;
            if(nextAvailableColumnSlot != location.getRow()) {
                isValid = false;
                log.debug("Chip location {} is out of order, trying to place at row {} when the next open slot is {}",
                        location.name(), location.getRow(), nextAvailableColumnSlot);
            }
        }
        return isValid;
    }

    private boolean isWinInList(List<List<LocationEntity>> winLines, PlayerColor winColor, boolean isRowOrder) {
        boolean isWin = false;
        for (List<LocationEntity> winLine : winLines) {
            isWin = isWin(winLine, winColor, isRowOrder);
            if (isWin) {
                break;
            }
        }
        return isWin;
    }

    // count the consecutive chips in the win line. watches for color changes and blank slots
    private boolean isWin(List<LocationEntity> winLine, PlayerColor winColor, boolean isRowOrder) {
        boolean isWin = false;
        if (winLine.size() >= 4) {
            int chipCount = 0;
            Integer lastPosition = -1;
            for (LocationEntity locationEntity : winLine) {
                Integer thisPosition = isRowOrder ? locationEntity.getChipLocation().getRow() : locationEntity.getChipLocation().getColumn();
                lastPosition = lastPosition < 0 ? thisPosition - 1 : lastPosition;
                if (winColor.equals(locationEntity.getPlayerColor()) && lastPosition + 1 == thisPosition) {
                    chipCount++;
                    if (chipCount == 4) {
                        isWin = true;
                        log.debug("win line = {}", winLine);
                        break; // exit inner loop
                    }
                } else {
                    chipCount = 0;
                }
                lastPosition = thisPosition;
            }
        }
        return isWin;
    }
}
