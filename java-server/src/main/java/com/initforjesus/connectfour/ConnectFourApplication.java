package com.initforjesus.connectfour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class to start Spring Boot application
 */
@SpringBootApplication
public class ConnectFourApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConnectFourApplication.class, args);
    }

}
