package com.initforjesus.connectfour.repository;

import com.initforjesus.connectfour.schema.entity.GameEntity;
import com.initforjesus.connectfour.schema.entity.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Spring JPA repository for database operations with the Location Entity
 */
@Validated
public interface LocationRepository extends JpaRepository<LocationEntity, Long> {
    // get an ordered list of locations for each win line
    List<LocationEntity> findAllByGameEntityAndColumnLineOrderByRowLine(@NotNull GameEntity gameEntity, @NotNull Integer columnLine);
    List<LocationEntity> findAllByGameEntityAndRowLineOrderByColumnLine(@NotNull GameEntity gameEntity,@NotNull Integer rowLine);
    List<LocationEntity> findAllByGameEntityAndRightDiagonalOrderByRowLine(@NotNull GameEntity gameEntity,@NotNull Integer rightDiagonal);
    List<LocationEntity> findAllByGameEntityAndLeftDiagonalOrderByRowLine(@NotNull GameEntity gameEntity, @NotNull Integer leftDiagonal);
}
