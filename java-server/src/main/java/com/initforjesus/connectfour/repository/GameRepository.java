package com.initforjesus.connectfour.repository;

import com.initforjesus.connectfour.schema.entity.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Spring JPA repository for database operations with the Game Entity
 */
public interface GameRepository extends JpaRepository<GameEntity, Long> {
    // finds the first game that is waiting for a second player
    Optional<GameEntity> findFirstByIsWaitingTrue();

    // find game with the session Id for either red or yellow
    default Optional<GameEntity> findBySessionId (@NotNull String sessionId) {
        return findDistinctByYellowSessionIdOrRedSessionId(sessionId, sessionId);
    }
    Optional<GameEntity> findDistinctByYellowSessionIdOrRedSessionId(@NotNull String yellowSessionId, @NotNull String redSessionId);
}
