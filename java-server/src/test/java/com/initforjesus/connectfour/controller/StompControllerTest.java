package com.initforjesus.connectfour.controller;

import com.initforjesus.connectfour.schema.dto.ChipLocation;
import com.initforjesus.connectfour.schema.dto.GameOverMessage;
import com.initforjesus.connectfour.schema.dto.MoveRequestDto;
import com.initforjesus.connectfour.schema.dto.MoveResponseDto;
import com.initforjesus.connectfour.schema.dto.NotificationType;
import com.initforjesus.connectfour.schema.dto.PlayerColor;
import com.initforjesus.connectfour.schema.dto.StartGameRequestDto;
import com.initforjesus.connectfour.schema.dto.StartGameResponseDto;
import com.initforjesus.connectfour.schema.dto.UserNotificationDto;
import com.initforjesus.connectfour.schema.entity.GameEntity;
import com.initforjesus.connectfour.service.GameService;
import com.initforjesus.connectfour.stomp.StompController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest
public class StompControllerTest {
    private static final String SESSION_ID = "ABCD-1234";
    private static final String USER_ID = "1234-ABCD";
    private static final String OTHER_USER_ID = "4321-DCBA";
    private static final String ALERT_QUEUE = "/queue/alert";
    private static final String START_QUEUE = "/queue/start";
    private static final String MOVE_QUEUE = "/queue/move";
    private static final Long GAME_ID = 1L;

    @InjectMocks
    private StompController stompController;

    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;
    @Mock
    private GameService gameService;
    @Mock
    private SessionDisconnectEvent disconnectEvent;
    @Mock
    private SimpMessageHeaderAccessor headerAccessor;

    @Captor
    private ArgumentCaptor<UserNotificationDto> userNotificationCaptor;
    @Captor
    private ArgumentCaptor<StartGameResponseDto> startGameResponseCaptor;
    @Captor
    private ArgumentCaptor<MoveResponseDto> moveResponseCaptor;

    private GameEntity gameEntity;

    @BeforeEach
    public void setUp() {
        gameEntity = GameEntity.builder()
                .id(GAME_ID)
                .yellowUserId(USER_ID)
                .yellowSessionId(SESSION_ID)
                .LocationEntities(new ArrayList<>())
                .build();

        when(disconnectEvent.getSessionId()).thenReturn(SESSION_ID);
        when(headerAccessor.getSessionId()).thenReturn(SESSION_ID);
    }

    @Test
    public void testOnApplicationEventSuccessOtherUser() {
        gameEntity.setIsWaiting(false);
        gameEntity.setRedUserId(OTHER_USER_ID);
        when(gameService.dropDisconnectedGame(SESSION_ID)).thenReturn(gameEntity);

        stompController.onApplicationEvent(disconnectEvent);

        verify(gameService).dropDisconnectedGame(SESSION_ID);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotification = userNotificationCaptor.getValue();
        assertNotNull(userNotification);
        assertEquals(GAME_ID, userNotification.getGameId());
        assertEquals(NotificationType.GAME_OVER, userNotification.getType());
        assertEquals(GameOverMessage.OPPONENT_QUIT, userNotification.getMessage());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testOnApplicationEventSuccessNoOtherUser() {
        gameEntity.setIsWaiting(false);
        when(gameService.dropDisconnectedGame(SESSION_ID)).thenReturn(gameEntity);

        stompController.onApplicationEvent(disconnectEvent);

        verify(gameService).dropDisconnectedGame(SESSION_ID);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotification = userNotificationCaptor.getValue();
        assertNotNull(userNotification);
        assertEquals(GAME_ID, userNotification.getGameId());
        assertEquals(NotificationType.ERROR, userNotification.getType());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testStartGameSuccess() {
        gameEntity.setIsWaiting(true);
        gameEntity.setNextMove(PlayerColor.YELLOW);
        StartGameRequestDto startGameRequestDto = StartGameRequestDto.builder().userId(USER_ID).build();
        when(gameService.processStartRequest(startGameRequestDto, SESSION_ID)).thenReturn(gameEntity);

        stompController.startGame(startGameRequestDto, headerAccessor);

        verify(gameService).processStartRequest(startGameRequestDto, SESSION_ID);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(START_QUEUE), startGameResponseCaptor.capture());
        StartGameResponseDto startGameResponse = startGameResponseCaptor.getValue();
        assertNotNull(startGameResponse);
        assertEquals(GAME_ID, startGameResponse.getGameId());
        assertEquals(USER_ID, startGameResponse.getUserId());
        assertEquals(PlayerColor.YELLOW, startGameResponse.getPlayerColor());
        assertEquals(PlayerColor.YELLOW, startGameResponse.getGoesFirst());
        assertTrue(startGameResponse.isWaiting());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testStartGameSuccessOpponentJoined() {
        gameEntity.setIsWaiting(false);
        gameEntity.setNextMove(PlayerColor.YELLOW);
        gameEntity.setRedUserId(OTHER_USER_ID);
        StartGameRequestDto startGameRequestDto = StartGameRequestDto.builder().userId(USER_ID).build();
        when(gameService.processStartRequest(startGameRequestDto, SESSION_ID)).thenReturn(gameEntity);

        stompController.startGame(startGameRequestDto, headerAccessor);

        verify(gameService).processStartRequest(startGameRequestDto, SESSION_ID);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(START_QUEUE), startGameResponseCaptor.capture());
        StartGameResponseDto startGameResponse = startGameResponseCaptor.getValue();
        assertNotNull(startGameResponse);
        assertFalse(startGameResponse.isWaiting());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.OPPONENT_JOINED, userNotificationDto.getType());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testStartGameSuccessYourMove() {
        gameEntity.setIsWaiting(false);
        gameEntity.setNextMove(PlayerColor.RED);
        gameEntity.setRedUserId(OTHER_USER_ID);
        StartGameRequestDto startGameRequestDto = StartGameRequestDto.builder().userId(USER_ID).build();
        when(gameService.processStartRequest(startGameRequestDto, SESSION_ID)).thenReturn(gameEntity);

        stompController.startGame(startGameRequestDto, headerAccessor);

        verify(gameService).processStartRequest(startGameRequestDto, SESSION_ID);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(START_QUEUE), any());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.YOUR_MOVE, userNotificationDto.getType());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testPlayerMoveSuccess() {
        gameEntity.setRedUserId(OTHER_USER_ID);
        ChipLocation chipLocation = ChipLocation.D7;
        MoveRequestDto moveRequestDto = MoveRequestDto.builder()
                .userId(USER_ID)
                .playerColor(PlayerColor.YELLOW)
                .chipLocation(chipLocation)
                .build();
        when(gameService.processMoveRequest(moveRequestDto, SESSION_ID)).thenReturn(gameEntity);
        when(gameService.checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW)).thenReturn("NONE");

        stompController.playerMove(moveRequestDto, headerAccessor);

        verify(gameService).processMoveRequest(moveRequestDto, SESSION_ID);
        verify(gameService).checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(MOVE_QUEUE), moveResponseCaptor.capture());
        MoveResponseDto moveResponse = moveResponseCaptor.getValue();
        assertNotNull(moveResponse);
        assertEquals(OTHER_USER_ID, moveResponse.getUserId());
        assertEquals(GAME_ID, moveResponse.getGameId());
        assertEquals(chipLocation, moveResponse.getChipLocation());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testPlayerMoveSuccessWin() {
        gameEntity.setRedUserId(OTHER_USER_ID);
        ChipLocation chipLocation = ChipLocation.D7;
        MoveRequestDto moveRequestDto = MoveRequestDto.builder()
                .userId(USER_ID)
                .playerColor(PlayerColor.YELLOW)
                .chipLocation(chipLocation)
                .build();
        when(gameService.processMoveRequest(moveRequestDto, SESSION_ID)).thenReturn(gameEntity);
        when(gameService.checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW)).thenReturn("YOU_WON");

        stompController.playerMove(moveRequestDto, headerAccessor);

        verify(gameService).processMoveRequest(moveRequestDto, SESSION_ID);
        verify(gameService).checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(MOVE_QUEUE), any());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.GAME_OVER, userNotificationDto.getType());
        assertEquals(GameOverMessage.YOU_WON, userNotificationDto.getMessage());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.GAME_OVER, userNotificationDto.getType());
        assertEquals(GameOverMessage.YOU_LOST, userNotificationDto.getMessage());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }

    @Test
    public void testPlayerMoveSuccessDraw() {
        gameEntity.setRedUserId(OTHER_USER_ID);
        ChipLocation chipLocation = ChipLocation.D7;
        MoveRequestDto moveRequestDto = MoveRequestDto.builder()
                .userId(USER_ID)
                .playerColor(PlayerColor.YELLOW)
                .chipLocation(chipLocation)
                .build();
        when(gameService.processMoveRequest(moveRequestDto, SESSION_ID)).thenReturn(gameEntity);
        when(gameService.checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW)).thenReturn("DRAW");

        stompController.playerMove(moveRequestDto, headerAccessor);

        verify(gameService).processMoveRequest(moveRequestDto, SESSION_ID);
        verify(gameService).checkForWinCondition(gameEntity, chipLocation, PlayerColor.YELLOW);
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(MOVE_QUEUE), any());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        UserNotificationDto userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.GAME_OVER, userNotificationDto.getType());
        assertEquals(GameOverMessage.DRAW, userNotificationDto.getMessage());
        verify(simpMessagingTemplate).convertAndSendToUser(eq(OTHER_USER_ID), eq(ALERT_QUEUE), userNotificationCaptor.capture());
        userNotificationDto = userNotificationCaptor.getValue();
        assertNotNull(userNotificationDto);
        assertEquals(NotificationType.GAME_OVER, userNotificationDto.getType());
        assertEquals(GameOverMessage.DRAW, userNotificationDto.getMessage());
        verifyNoMoreInteractions(gameService);
        verifyNoMoreInteractions(simpMessagingTemplate);
    }
}
