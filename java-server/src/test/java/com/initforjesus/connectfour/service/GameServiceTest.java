package com.initforjesus.connectfour.service;

import com.initforjesus.connectfour.repository.GameRepository;
import com.initforjesus.connectfour.repository.LocationRepository;
import com.initforjesus.connectfour.schema.dto.ChipLocation;
import com.initforjesus.connectfour.schema.dto.MoveRequestDto;
import com.initforjesus.connectfour.schema.dto.PlayerColor;
import com.initforjesus.connectfour.schema.dto.StartGameRequestDto;
import com.initforjesus.connectfour.schema.entity.GameEntity;
import com.initforjesus.connectfour.schema.entity.LocationEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest
public class GameServiceTest {
    private static final String SESSION_ID = "ABCD-1234";
    private static final String USER_ID = "1234-ABCD";
    private static final Long GAME_ID = 1L;

    @InjectMocks
    private GameService gameService;

    @Mock
    private GameRepository gameRepository;
    @Mock
    private LocationRepository locationRepository;

    private GameEntity gameEntity;
    private ChipLocation moveLocation;
    @BeforeEach
    public void setup() {
        gameEntity = GameEntity.builder()
                .id(GAME_ID)
                .yellowUserId(USER_ID)
                .yellowSessionId(SESSION_ID)
                .LocationEntities(new ArrayList<>())
                .build();

        moveLocation = ChipLocation.D7;
        when(locationRepository.findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, moveLocation.getColumn()))
                .thenReturn(new ArrayList<>());
        when(locationRepository.findAllByGameEntityAndRightDiagonalOrderByRowLine(gameEntity,moveLocation.getRightDiagonal()))
                .thenReturn(new ArrayList<>());
    }

    @Test
    public void testDropDisconnectedGameSuccess() {
        when(gameRepository.findBySessionId(SESSION_ID)).thenReturn(Optional.ofNullable(gameEntity));

        GameEntity result = gameService.dropDisconnectedGame(SESSION_ID);

        assertNotNull(result);
        assertEquals(GAME_ID, result.getId());
        verify(gameRepository).findBySessionId(SESSION_ID);
        verify(gameRepository).delete(gameEntity);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    public void testDropDisconnectedGameNoGame() {
        when(gameRepository.findBySessionId(SESSION_ID)).thenReturn(Optional.ofNullable(null));

        GameEntity result = gameService.dropDisconnectedGame(SESSION_ID);

        assertNull(result);
        verify(gameRepository).findBySessionId(SESSION_ID);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    public void testProcessStartRequestSuccessNewGame() {
        StartGameRequestDto startGameRequestDto = StartGameRequestDto.builder().userId(USER_ID).build();
        when(gameRepository.findFirstByIsWaitingTrue()).thenReturn(Optional.ofNullable(null));

        GameEntity result = gameService.processStartRequest(startGameRequestDto, SESSION_ID);

        assertNotNull(result);
        assertTrue(result.getIsWaiting());
        verify(gameRepository).findFirstByIsWaitingTrue();
        verify(gameRepository).save(result);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    public void testProcessStartRequestSuccessExistingGame() {
        StartGameRequestDto startGameRequestDto = StartGameRequestDto.builder().userId("OtheUser").build();
        gameEntity.setNextMove(PlayerColor.RED);
        when(gameRepository.findFirstByIsWaitingTrue()).thenReturn(Optional.ofNullable(gameEntity));

        GameEntity result = gameService.processStartRequest(startGameRequestDto, SESSION_ID);

        assertNotNull(result);
        assertFalse(result.getIsWaiting());
        assertEquals(GAME_ID, result.getId());
        verify(gameRepository).findFirstByIsWaitingTrue();
        verify(gameRepository).save(result);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    public void testProcessMoveRequest() {
        MoveRequestDto moveRequestDto = MoveRequestDto.builder()
                .gameId(GAME_ID)
                .userId(USER_ID)
                .chipLocation(ChipLocation.A1)
                .playerColor(PlayerColor.YELLOW)
                .build();
        gameEntity.setNextMove(PlayerColor.YELLOW);
        when(gameRepository.findById(GAME_ID)).thenReturn(Optional.ofNullable(gameEntity));
        when(locationRepository.findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, ChipLocation.A1.getColumn()))
                .thenReturn(new ArrayList<>());

        GameEntity result = gameService.processMoveRequest(moveRequestDto, SESSION_ID);

        assertNotNull(result);
        assertEquals(GAME_ID, result.getId());
        assertNotNull(result.getLocationEntities());
        assertNotNull(result.getLocationEntities().get(0));
        assertNotNull(result.getLocationEntities().get(0).getChipLocation());
        assertEquals(result.getLocationEntities().get(0).getChipLocation(), ChipLocation.A1);
        verify(gameRepository).findById(GAME_ID);
        verify(gameRepository).save(result);
        verify(locationRepository).findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, ChipLocation.A1.getColumn());
        verifyNoMoreInteractions(gameRepository);
        verifyNoMoreInteractions(locationRepository);
    }

    @Test
    public void testProcessMoveRequestLocationOutOfOrder() {
        MoveRequestDto moveRequestDto = MoveRequestDto.builder()
                .gameId(GAME_ID)
                .userId(USER_ID)
                .chipLocation(ChipLocation.A1)
                .playerColor(PlayerColor.YELLOW)
                .build();
        gameEntity.setNextMove(PlayerColor.YELLOW);
        when(gameRepository.findById(GAME_ID)).thenReturn(Optional.ofNullable(gameEntity));
        when(locationRepository.findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, ChipLocation.A1.getColumn()))
                .thenReturn(List.of(LocationEntity.builder().build(), LocationEntity.builder().build()));

        GameEntity result = gameService.processMoveRequest(moveRequestDto, SESSION_ID);

        assertNull(result);
        verify(gameRepository).findById(GAME_ID);
        verify(locationRepository).findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, ChipLocation.A1.getColumn());
        verifyNoMoreInteractions(gameRepository);
        verifyNoMoreInteractions(locationRepository);
    }

    @Test
    public void testCheckForWinConditionNone () {
        when(locationRepository.findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity,moveLocation.getRow()))
                .thenReturn(List.of( // 4 in row but separated by blank slots
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.A7).columnLine(1).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.D7).columnLine(4).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.E7).columnLine(5).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.F7).columnLine(6).build()
                ));

        String result = gameService.checkForWinCondition(gameEntity, moveLocation, PlayerColor.YELLOW);

        assertNotNull(result);
        assertEquals("NONE",result);
        verify(locationRepository).findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity, moveLocation.getRow());
        verify(locationRepository).findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, moveLocation.getColumn());
        verify(locationRepository).findAllByGameEntityAndRightDiagonalOrderByRowLine(gameEntity, moveLocation.getRightDiagonal());
    }

    @Test
    public void testCheckForWinConditionWin () {
        when(locationRepository.findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity,moveLocation.getRow()))
                .thenReturn(List.of( // 4 in row but separated by blank slots
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.A7).columnLine(1).build(),
                        LocationEntity.builder().playerColor(PlayerColor.RED).chipLocation(ChipLocation.B7).columnLine(2).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.C7).columnLine(3).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.D7).columnLine(4).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.E7).columnLine(5).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.F7).columnLine(6).build()
                ));

        String result = gameService.checkForWinCondition(gameEntity, moveLocation, PlayerColor.YELLOW);

        assertNotNull(result);
        assertEquals("YOU_WON",result);
        verify(locationRepository).findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity, moveLocation.getRow());
        verify(locationRepository).findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, moveLocation.getColumn());
        verify(locationRepository).findAllByGameEntityAndRightDiagonalOrderByRowLine(gameEntity, moveLocation.getRightDiagonal());
    }

    @Test
    public void testCheckForWinConditionDraw () {
        when(locationRepository.findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity,moveLocation.getRow()))
                .thenReturn(List.of( // 4 in row but separated by blank slots
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.A7).columnLine(1).build(),
                        LocationEntity.builder().playerColor(PlayerColor.RED).chipLocation(ChipLocation.B7).columnLine(2).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.C7).columnLine(3).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.D7).columnLine(4).build(),
                        LocationEntity.builder().playerColor(PlayerColor.RED).chipLocation(ChipLocation.E7).columnLine(5).build(),
                        LocationEntity.builder().playerColor(PlayerColor.YELLOW).chipLocation(ChipLocation.F7).columnLine(6).build()
                ));

        String result = gameService.checkForWinCondition(gameEntity, moveLocation, PlayerColor.YELLOW);

        assertNotNull(result);
        assertEquals("DRAW",result);
        verify(locationRepository).findAllByGameEntityAndRowLineOrderByColumnLine(gameEntity, moveLocation.getRow());
        verify(locationRepository).findAllByGameEntityAndColumnLineOrderByRowLine(gameEntity, moveLocation.getColumn());
        verify(locationRepository).findAllByGameEntityAndRightDiagonalOrderByRowLine(gameEntity, moveLocation.getRightDiagonal());
    }
}
